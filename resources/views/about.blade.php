@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="m-b-md text-center">
					<div class="title align-items-center pt-5">
						<div class="display-4 p-4">
							About Me - Victor Meza
						</div>
					</div>															<hr />
					<div class="h5 p-2">
					Hello! I'm Víctor Meza. Nice to see you on my blog. <br />
					I'm a Software Engineer and Senior Web developer. <br />
					This is a Web developer assignment for you	<br />
					I hope to pass it and join to the Square1 world wide team!
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
