<div class="container-fluid" id="footer">
	<div class="bottom-links">
		@if (Request::path() !== "blog")
			<a href="/">Blog</a>
		@endif
		@if (Request::path() !== "about")
			<a href="/about">About</a>
		@endif
		<a target="_blank" href="https://bitbucket.org/viistorrr/blog/src">Bitbucket</a>
		<hr>
		<div id="copyright text-right">© 2020. All rights reserved.</div>
	</div>
</div>
<script href="{{ asset('js/app.js') }}" rel="stylesheet"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@stack('inline-scripts')
