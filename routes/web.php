<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;

Auth::routes();

Route::resource('posts', 'PostsController');

Route::resource('comments', 'CommentsController', ['only' => ['store', 'destroy']]);

Route::get('/', 'PostsController@index')->name('home');

Route::get('/feed', 'HomeController@feed')->name('feed');

Route::get('/about', 'HomeController@about')->name('about');

Route::get('/api-posts', function (GuzzleHttp\Client $client){
	$response = $client->request('GET', "posts");
	$data = json_decode($response->getBody());
	return json_encode($data);
});

Route::group(['middleware' => 'auth'], function() {
	Route::resource('users', 'UsersController', ['only' => ['show','create','edit', 'update']]);
});

Route::group(['prefix' => 'auth'], function () {
	Route::get('{provider}', 'Auth\AuthController@redirectToProvider')->name('auth.provider');
	Route::get('{provider}/callback', 'Auth\AuthController@handleProviderCallback');
});

