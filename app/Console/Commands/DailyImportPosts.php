<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Post;
use Auth;

class DailyImportPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailyimport:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily auto import posts from external ENDPOINT';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET','https://sq1-api-test.herokuapp.com/posts');
        
        $posts = $response->getBody()->getContents();
        $posts =  json_decode($posts,true);
      
        foreach($posts as $post) {
            foreach($post as $po){
               
                $p = new Post;
                $p->title = $po['title'];
                $p->content = $po['description'];
                $p->author_id = 1;
                $p->save();
            }
        }       
        
    }
}
