<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects.
Laravel is accessible, powerful, and provides tools required for large, robust applications.

# Web Deveñpér Assignement - Victor Meza

Please follow the following instructions: 
1. Clone the repository
2. Run composer install command
3. Rename the .env-example to .env
4. Config your local database into your localhosts
5. Run php artisan migrate command
6. Run php artisan db:seed
7. Register a new User into de system and login
8. Run php artisan dailyimport:posts command
9. Run php artisan schedule:run for the Cronjob schedule


My name is Víctor Meza, a Web Developer from Colombia with 6 years of experience.
I hope the assignement its ok for the applications.

Thanks!

Thanks for the oportunity and I hope to receive your feedback

Víctor
## Email:## victormeza41@gmail.com